# OBJECT DETECTION

**Object detection** is a computer vision technique that allows us to **identify and locate objects in an image or video**. With this kind of identification and localization, object detection can be used to count objects in a scene and determine and track their precise locations, all while accurately labeling them.

Imagine, for example, an image that contains two cats and a person. Object detection allows us to at once classify the types of things found while also locating instances of them within the image.

<img src = "https://gitlab.com/kanchitank/module2-master-basic-skills/-/raw/master/images/8.jpg">

## What is object detection?

**Object detection** is a computer vision technique that works to **identify and locate objects within an image or video**. Specifically, object detection draws bounding boxes around these detected objects, which allow us to locate where said objects are in (or how they move through) a given scene.

**Object detection is commonly confused with image recognition**, so before we proceed, it’s important that we clarify the distinctions between them.

Image recognition assigns a label to an image. A picture of a dog receives the label “dog”. A picture of two dogs, still receives the label “dog”. Object detection, on the other hand, draws a box around each dog and labels the box “dog”. The model predicts where each object is and what label should be applied. In that way, object detection provides more information about an image than recognition.

Here’s an example of how this distinction looks in practice:<br>

<img src = "https://gitlab.com/kanchitank/module2-master-basic-skills/-/raw/master/images/9.jpg" height="400">

## Modes and types of object detection
Broadly speaking, object detection can be broken down into **machine learning-based approaches** and **deep learning-based approaches**.

In more traditional ML-based approaches, computer vision techniques are used to look at various features of an image, such as the color histogram or edges, to identify groups of pixels that may belong to an object. These features are then fed into a regression model that predicts the location of the object along with its label.

On the other hand, deep learning-based approaches employ convolutional neural networks (CNNs) to perform end-to-end, unsupervised object detection, in which features don’t need to be defined and extracted separately.

## Model architecture overview
#### R-CNN, Faster R-CNN, Mask R-CNN
A number of popular object detection models belong to the R-CNN family. Short for region convolutional neural network, these architectures are based on the region proposal structure discussed above. Over the years, they’ve become both more accurate and more computationally efficient. Mask R-CNN is the latest iteration, developed by researchers at Facebook, and it makes a good starting point for server-side object detection models.

#### CenterNet
More recently, researchers have developed object detection models that do away with the need for region proposals entirely. CenterNet treats objects as single points, predicting the X, Y coordinates of an object’s center and its extent (height and width). This technique has proven both more efficient and accurate than SSD or R-CNN approaches.
<br><br>
<img src = "https://gitlab.com/kanchitank/module2-master-basic-skills/-/raw/master/images/10.jpg" width="700">

## Object detection can be applied in a number of ways:
- **Crowd counting**
- **Self-driving cars**
- **Video surveillance**
- **Face detection**
- **Anomaly detection** (i.e. in industries like agriculture, health care)<br><br>


<img src = "https://gitlab.com/kanchitank/module2-master-basic-skills/-/raw/master/images/11.png" width="700"><br>

<img src = "https://gitlab.com/kanchitank/module2-master-basic-skills/-/raw/master/images/12.jpg" width="700">



