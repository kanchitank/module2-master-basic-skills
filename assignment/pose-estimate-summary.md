# POSE ESTIMATION

Pose estimation is a computer vision technique that predicts and tracks the location of a person or object. <br>
This is done by looking at a combination of the pose and the orientation of a given person/object.

## What is pose estimation?

Pose estimation is a computer vision task that infers the pose of a person or object in an mage or video. We can also think of pose estimation as the problem of determining the position and orientation of a camera relative to a given person or object.

This is typically done by identifying, locating, and tracking a number of keypoints on a given object or person. For objects, this could be corners or other significant features. And for humans, these keypoints represent major joints like an elbow or knee.

<img src = "https://gitlab.com/kanchitank/module2-master-basic-skills/-/raw/master/images/1.jpg">

#### In addition to tracking human movement and activity, pose estimation opens up applications in a range of areas, such as:
- Augmented reality
- Animation
- Gaming
- Robotics

## How does pose estimation work?

### Primary techniques for pose estimation
In general, deep learning architectures suitable for pose estimation are based on variations of convolutional neural networks (CNNs).<br>
There are two overarching approaches: a ***bottom-up approach***, and a ***top-down approach***.

- With a ***bottom-up approach***, the model detects every instance of a particular keypoint (e.g. all left hands) in a given image and then attempts to assemble groups of keypoints into skeletons for distinct objects.

- A ***top-down approach*** is the inverse – the network first uses an object detector to draw a box around each instance of an object, and then estimates the keypoints within each cropped region.

<img src = "https://gitlab.com/kanchitank/module2-master-basic-skills/-/raw/master/images/2.jpg">

## Model architecture overview

#### Stacked-Hourglass networks, Mask-RCNN, and other encoder-decoder networks

Pure encoder-decoder networks take an image as input and output heatmaps for each keypoint. If we need to detect multiple poses, Mask-RCNN is a versatile architecture that predicts bounding boxes for objects in an image and then predicts poses within the regions of the image enclosed in the box.

<img src = "https://gitlab.com/kanchitank/module2-master-basic-skills/-/raw/master/images/3.jpg">

#### Convolutional Pose Machines

Convolutional pose machines build on the encoder-decoder architecture by iteratively refining heatmap predictions using additional network layers and feature extraction. The final output is a single set of heatmaps, and post-processing involves identifying the pixels at which the heatmap probability is the highest for each keypoint.
